package ru.toppink.common.securities;

import ru.toppink.common.securities.dao.User;

public interface UserFactory {

    User getUser(String login);
}
