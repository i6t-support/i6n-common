package ru.toppink.common.securities;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.toppink.common.securities.dao.CustomUserDetails;
import ru.toppink.common.securities.dao.User;

@Component
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserFactory userFactory;

    @Override
    public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userEntity = userFactory.getUser(username);
        return CustomUserDetails.fromUserEntityToCustomUserDetails(userEntity);
    }
}