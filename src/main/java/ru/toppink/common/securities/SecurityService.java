package ru.toppink.common.securities;

import java.util.Set;

public interface SecurityService {

    String getLogin();

    Set<String> getRoles();

    String encodePassword(String password);

    boolean matchPassword(String raw, String password);
}
