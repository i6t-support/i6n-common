package ru.toppink.common.securities.dao;

import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.toppink.common.securities.SecurityService;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

public class CustomUserDetails implements UserDetails {

    private String login;
    private String password;

    @Setter
    private Collection<? extends GrantedAuthority> grantedAuthorities = new HashSet<>();

    public static CustomUserDetails fromUserEntityToCustomUserDetails(User userEntity) {
        CustomUserDetails details = new CustomUserDetails();
        details.login = userEntity.getLogin();
        details.password = userEntity.getPassword();
        //details.grantedAuthorities = Collections.singletonList();
        return details;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}