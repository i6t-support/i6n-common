package ru.toppink.common.securities.dao;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import nonapi.io.github.classgraph.json.Id;
import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserRole {

    @Id
    private UUID id;

    private String name;
}
