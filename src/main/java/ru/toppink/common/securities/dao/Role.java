package ru.toppink.common.securities.dao;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;

@RequiredArgsConstructor
public enum Role {


    USER("ROLE_USER"),
    OPERATOR("ROLE_OPERATOR"),
    TECH_SPEC("ROLE_TECH_SPEC"),
    ADMIN("ROLE_ADMIN"),
    SUPPORT("ROLE_SUPPORT"),
    TUZ("TUZ");

    @Getter
    private final String raw;
}
