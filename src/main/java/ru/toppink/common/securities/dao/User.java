package ru.toppink.common.securities.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private UUID id;

    private String login;

    private String password;

    private String firstName;

    private String secondName;

    private String patronymic;

    private UserRole role;
}
