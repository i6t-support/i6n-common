package ru.toppink.common.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;
import ru.toppink.common.config.ObjectMapperConfig;
import java.util.Map;

@Slf4j
public class SimpleDeserializer implements Deserializer<Object> {

    private ObjectMapper objectMapper;
    private String encoding = "UTF8";

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        String propertyName = isKey ? "key.deserializer.encoding" : "value.deserializer.encoding";
        Object encodingValue = configs.get(propertyName);
        if (encodingValue == null)
            encodingValue = configs.get("deserializer.encoding");
        if (encodingValue instanceof String)
            encoding = (String) encodingValue;
    }

    @Override
    public Object deserialize(String topic, byte[] data) {
        try {
            if (data == null) {
                return null;
            } else {
                String rawData = new String(data, encoding);
                Map<String, Object> map = getObjectMapper().readValue(rawData, Map.class);
                Object object = getObjectMapper().readValue(
                        objectMapper.writeValueAsString(map.get("content")),
                        Class.forName((String) map.get("aclass"))
                );
                return object;
            }
        } catch (Exception e) {
            log.error("Error: {}", e.getMessage());
            return new SerializationException("Error when deserializing byte[] to string due to unsupported encoding " + encoding);
        }
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper == null ? objectMapper = new ObjectMapperConfig().objectMapper() : objectMapper;
    }
}
