package ru.toppink.common.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import ru.toppink.common.exception.OrdinalFieldException;
import java.lang.reflect.Field;

@Slf4j
@Profile("!test")
@RequiredArgsConstructor
public class KafkaSender {

    private final KafkaTemplate<String, Object> template;

    @Value("${kafka.topics.user-out:user-out}")
    private String userOut;
    @Value("${kafka.topics.dispatcher-out:dispatcher-out}")
    private String dispatcherOut;
    @Value("${kafka.topics.application-out:application-out}")
    private String applicationOut;
    @Value("${kafka.topics.notification-out:notification-out}")
    private String notificationOut;

    public void sendToApplicationOut(Object message) {
        log.debug("Kafka try write to application-out: {}", message);
        tryWrite(applicationOut, message);
        log.debug("kafka successful write to applicationOut");
    }

    public void sendToNotificationOut(Object message) {
        log.debug("Kafka try write to notification-out: {}", message);
        tryWrite(notificationOut, message);
        log.debug("kafka successful write to notification-out");
    }

    public void sendToDispatcherOut(Object message) {
        log.debug("Kafka try write to dispatcher-out: {}", message);
        tryWrite(dispatcherOut, message);
        log.debug("kafka successful write to dispatcher-out");
    }

    public void sendToUserOut(Object message) {
        log.debug("Kafka try write to user-out: {}", message);
        tryWrite(userOut, message);
        log.debug("kafka successful write to user-out");
    }

    private void tryWrite(String topic, Object object) {
        if (topic == null) {
            log.debug("Topic name is null");
        } else if (object == null) {
            log.debug("Message is null");
        } else if (template == null) {
            log.error("Template not found for write");
        } else {
            String key = extractKey(object);
            log.debug("Resolve key for this message: {}", key);
            template.send(topic, key, object);
        }
    }

    private String extractKey(Object obj) {
        final String clazzName = obj.getClass().getSimpleName();

        if (obj instanceof OrdinalEvent) {
            String key = null;

            for (Field field : obj.getClass().getDeclaredFields()) {
                OrdinalId declaredAnnotation = field.getDeclaredAnnotation(OrdinalId.class);
                if (declaredAnnotation != null) {
                    if (key == null) {
                        try {
                            field.setAccessible(true);
                            key = field.get(obj).toString();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    } else {
                        throw new OrdinalFieldException(
                                "Two or many field have annotation @OrdinalId, expected: 1. " +
                                        "Message: " + clazzName);
                    }
                }
            }

            if (key == null) {
                throw new OrdinalFieldException(
                        "Class " + clazzName + " implements OrdinalEvent should contains @OrdinalId");
            }

            return key;

        } else {
            return clazzName;
        }
    }
}
