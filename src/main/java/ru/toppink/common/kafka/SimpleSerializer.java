package ru.toppink.common.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jca.context.SpringContextResourceAdapter;
import ru.toppink.common.config.ObjectMapperConfig;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import javax.annotation.PostConstruct;

public class SimpleSerializer implements Serializer<Object> {

    private ObjectMapper objectMapper;

    private String encoding = "UTF8";

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        String propertyName = isKey ? "key.serializer.encoding" : "value.serializer.encoding";
        Object encodingValue = configs.get(propertyName);
        if (encodingValue == null)
            encodingValue = configs.get("serializer.encoding");
        if (encodingValue instanceof String)
            encoding = (String) encodingValue;
    }

    @Override
    public byte[] serialize(String topic, Object data) {
        try {
            if (data == null) {
                return null;
            } else {
                BoxedJson boxedJson = new BoxedJson(data, data.getClass());
                return getObjectMapper().writeValueAsString(boxedJson).getBytes(encoding);
            }
        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            throw new SerializationException("Error when serializing string to byte[] due to unsupported encoding " + encoding);
        }
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper == null ? objectMapper = new ObjectMapperConfig().objectMapper() : objectMapper;
    }
}
