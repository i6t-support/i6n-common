package ru.toppink.common.config;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaAdmin;
import java.util.HashMap;
import java.util.Map;

@Configuration
@Profile("!test")
public class KafkaTopicConfig {
    
    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return new KafkaAdmin(configs);
    }
    
    @Bean
    public NewTopic applicationOut() {
         return new NewTopic("application-out", 1, (short) 1);
    }

    @Bean
    public NewTopic userOut() {
         return new NewTopic("user-out", 1, (short) 1);
    }

    @Bean
    public NewTopic notificationOut() {
         return new NewTopic("notification-out", 1, (short) 1);
    }

    @Bean
    public NewTopic dispatcherOut() {
         return new NewTopic("dispatcher-out", 1, (short) 1);
    }
}