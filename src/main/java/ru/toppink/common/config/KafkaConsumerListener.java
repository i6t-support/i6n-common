package ru.toppink.common.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
@Configuration
@RequiredArgsConstructor
@Profile("!test")
public class KafkaConsumerListener {

    private final ApplicationEventPublisher applicationEventPublisher;

    @KafkaListener(
            topics = "${kafka.topics.application-out:application-out}",
            autoStartup = "${kafka.subscribe.application-out:false}"
    )
    public void listenApplicationOut(ConsumerRecord<String, Object> record) {
        Object event = record.value();
        if (event instanceof RuntimeException) {
            ((Exception) event).printStackTrace();
        } else {
            log.debug("Received Message from application-out: {}", event);
            applicationEventPublisher.publishEvent(new PayloadApplicationEvent<>(record.key(), event));
        }
    }

    @KafkaListener(
            topics = "${kafka.topics.notification-out:notification-out}",
            autoStartup = "${kafka.subscribe.notification-out:false}"
    )
    public void listenNotificationOut(ConsumerRecord<String, Object> record) {
        Object event = record.value();
        if (event instanceof RuntimeException) {
            ((Exception) event).printStackTrace();
        } else {
            log.debug("Received Message from notification-out: {}", event);
            applicationEventPublisher.publishEvent(new PayloadApplicationEvent<>(record.key(), event));
        }
    }

    @KafkaListener(
            topics = "${kafka.topics.user-out:user-out}",
            autoStartup = "${kafka.subscribe.user-out:false}"
    )
    public void listenUserOut(ConsumerRecord<String, Object> record) {
        Object event = record.value();
        if (event instanceof RuntimeException) {
            ((Exception) event).printStackTrace();
        } else {
            log.debug("Received Message from user-out: {}", event);
            applicationEventPublisher.publishEvent(new PayloadApplicationEvent<>(record.key(), event));
        }
    }

    @KafkaListener(
            topics = "${kafka.topics.dispatcher-out:dispatcher-out}",
            autoStartup = "${kafka.subscribe.dispatcher-out:false}"
    )
    public void listenDispatcherOut(ConsumerRecord<String, Object> record) {
        Object event = record.value();
        if (event instanceof RuntimeException) {
            ((Exception) event).printStackTrace();
        } else {
            log.debug("Received Message from dispatcher-out: {}", event);
            applicationEventPublisher.publishEvent(new PayloadApplicationEvent<>(record.key(), event));
        }
    }
}
