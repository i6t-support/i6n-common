package ru.toppink.common.utils;

import ru.toppink.common.exception.SimpleException;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Optionals {

    /**
     * Simple static constructor
     *
     * @param <T> type option value
     * @param value option value
     * @param consumer action for if is not empty
     * @return return option object if present
     */
    public static <T> T of(
            T value,
            Consumer<T> consumer) {
        if (value != null) {
            consumer.accept(value);
        }
        return value;
    }

    /**
     * Simple static constructor
     *
     * @param value option value
     * @param ifPresent action for if is not empty
     * @param ifEmpty action for if is empty
     * @param <T> type option values
     * @return return option object if present
     */
    public static <T> T  of(
            T value,
            Consumer<T> ifPresent,
            Supplier<SimpleException> ifEmpty
    ) throws SimpleException {
        if (value == null) {
            throw ifEmpty.get();
        } else {
            ifPresent.accept(value);
            return value;
        }
    }


    /**
     * Simple static constructor
     *
     * @param value option value
     * @param ifPresent action for if is not empty
     * @param ifEmpty action for if is empty
     * @param <T> type option values
     * @return return option object if present
     */
    public static <T> T  ofOpt(
            Optional<T> value,
            Consumer<T> ifPresent,
            Supplier<SimpleException> ifEmpty
    ) throws SimpleException {
        T present = value.orElseThrow(ifEmpty);
        ifPresent.accept(present);
        return present;
    }

    /**
     * Simple static constructor
     *
     * @param value option value
     * @param ifPresent action for if is not empty
     * @param <T> type option values
     * @return return option object if present
     */
    public static <T> T of(
            Optional<T> value,
            Consumer<T> ifPresent
    ) throws Throwable {
        T present = value.orElseThrow();
        ifPresent.accept(present);
        return present;
    }
}
