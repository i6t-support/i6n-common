package ru.toppink.common.utils;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Value;

public class TuzRequestInterceptor implements RequestInterceptor {

    @Value("${tuz}")
    private String tuz;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("Authorization: Bearer " + this.tuz);
    }
}
