package ru.toppink.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

public class OrdinalFieldException extends SimpleException {

    private static final String ERROR_MSG = "OrdinalManyFieldException: %s";

    public OrdinalFieldException(String message) {
        super(
                HttpStatus.INTERNAL_SERVER_ERROR,
                String.format(ERROR_MSG, StringUtils.isEmpty(message.trim()) ? "unknown error" : message)
        );
    }
}
