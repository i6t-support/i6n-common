package ru.toppink.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class UserNotFoundException extends NotFoundException {

    private static final String ERROR_MSG = "User with login/id = %s not found";

    public UserNotFoundException(String userIdOrLogin) {
        super(String.format(ERROR_MSG, userIdOrLogin));
    }
}
