package ru.toppink.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.util.Map;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(HttpStatusCodeException.class)
    public ResponseEntity<Object> handleHttpClientException(HttpStatusCodeException ex) {
        String message = ex.getMessage();
        HttpStatus statusCode = ex.getStatusCode();

        Map<String, String> body = Map.of(
                "message", message
        );

        return new ResponseEntity<>(body, statusCode);
    }

    @ExceptionHandler(SimpleException.class)
    public ResponseEntity<Object> handleSimpleExceptionException(SimpleException ex) {
        String message = ex.getMessage();
        HttpStatus statusCode = ex.getStatus();

        Map<String, Object> body = Map.of(
                "message", message,
                "status", statusCode.getReasonPhrase(),
                "class", ex.getClass().getSimpleName()
        );

        return new ResponseEntity<>(body, statusCode);
    }
}
