package ru.toppink.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import ru.toppink.common.exception.SimpleException;

public class NotFoundException extends SimpleException {

    private static final String ERROR_MSG = "Not found: %s";

    public NotFoundException(String message) {
        super(
                HttpStatus.NOT_FOUND,
                String.format(ERROR_MSG, StringUtils.isEmpty(message.trim()) ? "unknown error" : message)
        );
    }
}
