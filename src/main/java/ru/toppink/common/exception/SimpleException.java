package ru.toppink.common.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

public class SimpleException extends RuntimeException {

    private static final String ERROR_MSG =  "[class]: %s";

    @Getter
    private final String message;
    @Getter
    private final HttpStatus status;

    public SimpleException(HttpStatus status, String message) {
        String exceptionMessage = String.format(
                ERROR_MSG.replace("[class]", this.getClass().getSimpleName()),
                StringUtils.isEmpty(message.trim()) ? "unknown error" : message
        );

        this.status = status;
        this.message = exceptionMessage;
    }

    public SimpleException(String message) {
        this(HttpStatus.CONFLICT, message);
    }
}
