package ru.toppink.common.exception;

import ru.toppink.common.exception.SimpleException;

public class UserLoginAlreadyBusyException extends SimpleException {

    private static final String MSG = "User with login = %s already created";

    public UserLoginAlreadyBusyException(String login) {
        super(String.format(MSG, login));
    }
}
